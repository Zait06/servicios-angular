import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Articulo } from '../models/Articulo';
import { User } from '../models/User';
import { ArticulosService } from '../services/articulos.service';

@Component({
  selector: 'app-agregar-articulo',
  templateUrl: './agregar-articulo.component.html',
  styleUrls: ['./agregar-articulo.component.scss']
})
export class AgregarArticuloComponent implements OnInit {

  usuarios: Array<User> = new Array<User>();
  formularioArti: FormGroup;
  articulo: Articulo = new Articulo();
  esNuevo: boolean = true;

  constructor(private ArtiInyec: ArticulosService, private fbGenerador: FormBuilder, private RutaActiva: ActivatedRoute) {
    
  }

  ngOnInit() {
    this.esNuevo = JSON.parse(this.RutaActiva.snapshot.params['esNuevo']);
    let titulo = '';
    let cuerpo = '';
    let userId = 0;
    if (!this.esNuevo) {
      this.articulo = this.ArtiInyec.articulo;
      titulo = this.articulo.title;
      cuerpo = this.articulo.body;
      userId = this.articulo.userId;
    }
    this.formularioArti = this.fbGenerador.group({
      title: [titulo, Validators.required],
      body: [cuerpo, Validators.required],
      userId: [userId, Validators.required]
    });


    this.ArtiInyec.leerAllUsuarios().subscribe((usuariosFromAPI)=>{
      this.usuarios = usuariosFromAPI;
    });
  }

  agregar(){
    this.articulo = this.formularioArti.value as Articulo;
    this.ArtiInyec.guardarArticulo(this.articulo).subscribe((arti)=>{
      console.log(arti);
      console.log("Se ha registrado el articulo");
      this.formularioArti.reset();
    });
  }

  editar(){
    this.articulo = this.formularioArti.value as Articulo;
    this.articulo.id = this.ArtiInyec.articulo.id;
    this.ArtiInyec.actualizarArticulo(this.articulo).subscribe((arti)=>{
      console.log(arti);
      console.log("Se ha actualizado el articulo");
    });
  }

}