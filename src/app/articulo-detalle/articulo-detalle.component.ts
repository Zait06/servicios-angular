import { Component, OnInit } from '@angular/core';
import { Articulo } from '../models/Articulo';
import { User } from '../models/User';
import { ArticulosService } from '../services/articulos.service';

@Component({
  selector: 'app-articulo-detalle',
  templateUrl: './articulo-detalle.component.html',
  styleUrls: ['./articulo-detalle.component.scss']
})
export class ArticuloDetalleComponent implements OnInit {
  articulo: Articulo = new Articulo()
  usuario: User = new User();
  constructor(private ArtiInyec: ArticulosService) {
    this.articulo = this.ArtiInyec.articulo;
  }

  ngOnInit() {
    this.ArtiInyec.leerUsuario(this.articulo.id).subscribe((userFromAPI)=>{
      this.usuario = userFromAPI;
    });
  }

}
