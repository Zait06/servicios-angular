import { Injectable } from '@angular/core';
import { Usuario } from '../models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuario: Usuario = new Usuario();
  constructor() {
    this.usuario.usuarioID = 1;
    this.usuario.nombre = 'Tomas';
    this.usuario.apellido = 'Garay';
  }
}
