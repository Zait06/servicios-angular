import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Articulo } from '../models/Articulo';
import { ArticulosService } from '../services/articulos.service';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  articulos: Array<Articulo> = new Array<Articulo>();

  constructor(private UserInyec: UsuarioService, private ArtiInyec: ArticulosService, private Ruta: Router) {}

  ngOnInit() {
    this.ArtiInyec.leerNoticias().subscribe((artiFromAPI)=>{
      this.articulos = artiFromAPI;
    });

    let articuloEnviar: Articulo = new Articulo();
    articuloEnviar.body = "Este es el cuerpo";
    articuloEnviar.title = "Es una prueba";
    articuloEnviar.id = 2;
    articuloEnviar.userId = 2;
    this.ArtiInyec.guardarArticulo(articuloEnviar).subscribe((artiFromAPI)=>{
      this.articulos.push(artiFromAPI)
    });
  }

  irAlDetalle(arti: Articulo){
    this.ArtiInyec.articulo = arti;
    this.Ruta.navigateByUrl('/articulo-detalle');
  }

  borrar(id: number){
    this.ArtiInyec.borrarArticulo(id).subscribe((datos)=>{
      console.log(datos);
      console.log("Eliminado correctamente");
    });
  }

  actualizar(arti: Articulo){
    this.ArtiInyec.articulo = arti;
    this.Ruta.navigateByUrl('/agregar-articulo/false');
  }

}
